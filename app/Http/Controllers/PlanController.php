<?php

namespace FloorPlan\Http\Controllers;

use Illuminate\Http\Request;
use FloorPlan\Http\Requests;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the floors in the specific building.
     *
     * @param $building_slug
     * @return \Illuminate\Http\Response
     */
    public function showFloors($building_slug)
    {
        //
    }

    /**
     * Display the rooms on the specific floor.
     *
     * @param $building_slug
     * @param $floor_slug
     * @return \Illuminate\Http\Response
     */
    public function showRooms($building_slug, $floor_slug)
    {
        //
    }

    /**
     * Display the room resource.
     *
     * @param $building_slug
     * @param $floor_slug
     * @param $room_slug
     * @return \Illuminate\Http\Response
     */
    public function showRoom($building_slug, $floor_slug, $room_slug)
    {
        //
    }
}
