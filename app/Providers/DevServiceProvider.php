<?php

namespace FloorPlan\Providers;

use Illuminate\Support\ServiceProvider;

class DevServiceProvider extends ServiceProvider
{
    /**
     * Service provider for dev packages required for local development.
     *
     * @return void
     */
    public function register()
    {
        $dev_providers = [
            \Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,
            \Barryvdh\Debugbar\ServiceProvider::class,
            \Laracasts\Generators\GeneratorsServiceProvider::class,
        ];

        if ($this->app->environment('local')) {
            foreach ($dev_providers as $provider) {
                $this->app->register($provider);
            }
        }
    }
}
