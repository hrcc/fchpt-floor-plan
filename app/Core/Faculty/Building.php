<?php

namespace FloorPlan\Core\Faculty;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Building
 *
 * @package FloorPlan\Core\Faculty
 */
class Building extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['label'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function floors()
    {
        return $this->hasMany(Floor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function rooms()
    {
        return $this->hasManyThrough(Room::class, Floor::class);
    }
}
