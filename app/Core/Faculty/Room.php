<?php

namespace FloorPlan\Core\Faculty;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Room
 *
 * @package FloorPlan\Core\Faculty
 */
class Room extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['slug', 'ais_name', 'number', 'room_type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }
}
