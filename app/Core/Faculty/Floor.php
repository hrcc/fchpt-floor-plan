<?php

namespace FloorPlan\Core\Faculty;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Floor
 *
 * @package FloorPlan\Core\Faculty
 */
class Floor extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['label'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building()
    {
        return $this->belongsTo(Building::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }
}
